//
//  DataManager.swift
//  todoey2
//
//  Created by Sergio Escalante Ordonez on 27/1/21.
//

import UIKit.UIApplication
import CoreData

class DataManager {
    
    static let shared: DataManager = DataManager()
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func loadCategories(with request: NSFetchRequest<Category> = Category.fetchRequest()) -> [Category] {
        do {
            let categories = try context.fetch(request)
            return categories
        } catch {
            print("Error fetching categories: \(error)")
            return []
        }
    }
}
