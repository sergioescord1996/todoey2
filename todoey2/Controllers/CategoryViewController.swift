//
//  CategoryViewController.swift
//  todoey2
//
//  Created by Sergio Escalante Ordonez on 18/1/21.
//

import UIKit

class CategoryViewController: UITableViewController {
    
    // MARK: - Outlets
    
    // MARK: - Constants
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    // MARK: - Variables
    
    var categories = [Category]()
    
    // MARK: - Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCategories()
    }
    
    // MARK: - Actions
    @IBAction func addCategory(_ sender: UIBarButtonItem) {
    }
    
    // MARK: - TablewView Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constants.SegueIdentifier.ToDoList, sender: self)
    }

    // MARK: - TableView Datasource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.CellIdentifier.CategoryCell, for: indexPath)
        
        cell.textLabel?.text = categories[indexPath.row].name
        
        return cell
    }
    
    // MARK: - Data Manager Methods
    
    func loadCategories() {
        categories = DataManager.shared.loadCategories()
        sleep(2)
        tableView.reloadData()
    }
    
    // MARK: - Navigation Methods

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
