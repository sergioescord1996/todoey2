//
//  Constants.swift
//  todoey2
//
//  Created by Sergio Escalante Ordonez on 27/1/21.
//


struct Constants {
    
    static let shared = Constants()
    
    struct CellIdentifier {
        static let CategoryCell = "CategoryCell"
        static let TodoListCell = "TodolistCell"
    }
    
    struct SegueIdentifier {
        static let ToDoList = "goToList"
    }
}
